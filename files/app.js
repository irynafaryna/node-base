const fs = require('fs');
//readFile is an async function

fs.readFile('./test.txt', 'utf8', (error, data) => {
  console.log(data)// відобразиться буфер якщо не буде utf 8
  fs.mkdir('./newfile', () => {})

  fs.writeFile('./newfile/test2.txt', `${data} New text!`, error => {
    error ? console.log(error) : null;
  })
})

//видалення файлу
// setTimeout(()=> {
//   fs.unlink('./newfile/test2.txt', () => {})
// }, 3000);

// //видалення папки
// setTimeout(()=> {
//   fs.rmdir('./newfile', () => {})
// }, 5000);
