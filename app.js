const http = require('http');

const PORT = 3002;

const server = http.createServer((req, res) => {
  console.log('Server request');
  console.log(req.url, req.method);

  res.setHeader('Content-Type', 'text/plain');

  res.write('Hello world!');
  res.end();
})

server.listen(PORT, 'localhost', error => {
  error ? console.log(error) : console.log(`listening port ${PORT}`)
})