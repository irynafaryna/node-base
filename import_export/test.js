const userName = 'Iryna';

const sayHi = userName => `Hello, my name is ${userName}`;

module.exports = {
  sayHi,
  userName,
}