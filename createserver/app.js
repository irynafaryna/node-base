const http = require('http');

const PORT = 8080;

const server = http.createServer((req, res) => {
  console.log('Server request');
  console.log('Just for testing');
  console.log(req.url, req.method);

  res.setHeader('Content-Type', 'text/plain');

  res.write('Hello world!');
  res.end();
})

server.listen(PORT, 'localhost', error => {
  error ? console.log(error) : console.log('listening port 8080')
})