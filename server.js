const express = require('express');
const path = require('path');
const morgan = require('morgan')

const app = express();

const PORT = 3000;

const createPath = page => path.resolve(__dirname, 'create_routing/views', `${page}.html`);


app.listen(PORT, error => {
  error ? console.log(error) : console.log(`listening port ${PORT}`)
})

//middleware
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

app.get('/', (req, res) => {
  res.sendFile(createPath('index'));
})

app.get('/contact', (req, res) => {
  res.sendFile(createPath('contact'));
})

//якщо шлях на сторінку змінився, припускаємо що був about-us а хочемо contact
app.get('/about-us', (req, res) => {
  res.redirect('/contact');
})

//якщо ввели неіснуючу сторінку
app.use((req, res) => {
  res
  .status(404)
  .sendFile(createPath('error'));
})